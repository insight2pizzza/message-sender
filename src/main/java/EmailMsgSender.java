import exceptions.EmailException;
import exceptions.MessageLengthException;
import org.apache.commons.validator.routines.EmailValidator;

import java.util.List;

public class EmailMsgSender implements MessageSender {

    @Override
    public void send(List<String> emails, String msgText) throws MessageLengthException, EmailException {
        emailValidation(emails);
        messageTextValidation(msgText);
    }

    private void messageTextValidation(String messageText) throws MessageLengthException {
        if (messageText.length() > 350) {
            throw new MessageLengthException("The text length is more then 250 symbols");
        }
    }

    private void emailValidation(List<String> emails) throws EmailException {
        for (String email : emails) {
            if (!EmailValidator.getInstance().isValid(email)) {
                throw new EmailException("The email is invalid ", email);
            }
        }
    }

}
