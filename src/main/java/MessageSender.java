import exceptions.EmailException;
import exceptions.MessageLengthException;
import exceptions.PhoneNumbersException;

import java.util.List;

public interface MessageSender {
    void send(List<String> uniqIndentificator, String msgText) throws PhoneNumbersException, MessageLengthException, EmailException;
}
