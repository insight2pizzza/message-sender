import exceptions.EmailException;
import exceptions.MessageLengthException;
import exceptions.PhoneNumbersException;

import java.util.List;

public class Sender {
    private EmailMsgSender emailMsgSender = new EmailMsgSender();
    private PhoneMsgSender phoneMsgSender = new PhoneMsgSender();

    void sendViaEmail(List<String> emails, String msgText) throws MessageLengthException, EmailException {
        emailMsgSender.send(emails, msgText);
    }

    void sendViaPhone(List<String> phoneNumbers, String msgText) throws MessageLengthException, PhoneNumbersException {
        phoneMsgSender.send(phoneNumbers, msgText);
    }
}
