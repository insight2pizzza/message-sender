import exceptions.MessageLengthException;
import exceptions.PhoneNumbersException;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PhoneMsgSender implements MessageSender {

    @Override
    public void send(List<String> phoneNumbers, String messageText) throws PhoneNumbersException, MessageLengthException {
        phoneNumbersValidation(phoneNumbers);
        messageTextValidation(messageText);
        System.out.println("Message is sended");
    }

    private void phoneNumbersValidation(List<String> phoneNumbers) throws PhoneNumbersException {
        String patterns
                = "^(\\+\\d{1,3}( )?)?((\\(\\d{3}\\))|\\d{3})[- .]?\\d{3}[- .]?\\d{4}$"
                + "|^(\\+\\d{1,3}( )?)?(\\d{3}[ ]?){2}\\d{3}$"
                + "|^(\\+\\d{1,3}( )?)?(\\d{3}[ ]?)(\\d{2}[ ]?){2}\\d{2}$";
        Pattern pattern = Pattern.compile(patterns);

        for (String phoneNumber : phoneNumbers) {
            Matcher matcher = pattern.matcher(phoneNumber);
            if (!matcher.matches()) {
                throw new PhoneNumbersException("The phone number is wrong", phoneNumber);
            }
        }
    }

    private void messageTextValidation(String messageText) throws MessageLengthException {
        if (messageText.length() > 50) {
            throw new MessageLengthException("The text length is more then 250 symbols");
        }
    }
}
