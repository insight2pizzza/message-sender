package exceptions;

public class MessageLengthException extends Exception {

    public MessageLengthException(String message) {
        super(message);
    }
}