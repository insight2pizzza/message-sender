package exceptions;

public class EmailException extends Exception {
    public EmailException(String message, String email) {
        super(message + email);
    }
}
