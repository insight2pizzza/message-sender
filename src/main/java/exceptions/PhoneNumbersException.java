package exceptions;

public class PhoneNumbersException extends Exception {

    public PhoneNumbersException(String message, String number) {
        super(message + ": " + number);
    }
}